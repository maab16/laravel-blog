@extends('layout.app')

@section('title','About')

@section('body')
    @unless($messages)
        There is no data
    @endunless
    @foreach($messages as $message)
        {{$message}}
    @endforeach
@endsection
