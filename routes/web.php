<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','homeController@index');

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/about', function () {
	$messages = ['This','is','About','page'];
	/*
	 * Here is multiple options
	 * 1. ['key'=>$value]
	 * 2. compact('variableName');
	 * 3. with(['key'=>$value])
	 * 4. withVariableName($value)
	 */

	// Option 1
    //return view('about',['messages'=>$messages]); // about.blade.php
    // Option 2
    return view('about',compact('messages')); // about.blade.php
    // Option 3
    //return view('about')->with(['messages'=>$messages]); // about.blade.php
    // Option 4
    //return view('about')->withMessages($messages); // about.blade.php
});

Route::resource('songs','songsController');
