<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passport extends Model
{
    
    public function user(){

    	/*
 		 * Option One
 		 * return $this->belongsTo('\App\Passport');
    	 */

    	// Option Two
    	return $this->belongsTo(User::class);
    }
}
