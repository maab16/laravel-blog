<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    
    public function passport(){
    	/*
 		 * Option One
 		 * return $this->hasOne('\App\Passport');
    	 */
    	// Option Two
    	return $this->hasOne(Passport::class);
    }
}
